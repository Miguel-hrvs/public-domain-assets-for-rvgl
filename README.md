# public domain assets for rvgl

Assets licensed in public domain to be used in the rvgl project, because they still rely on the original re-volt ones.
Any help would be welcomed.
